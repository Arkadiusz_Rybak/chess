

public class Queen extends Piece {
    public Queen(String color, Position position,
                 boolean isAlive, boolean hasMove, Board board) {
        super(color, position, isAlive, hasMove, board);
    }


    @Override
    boolean move(Position destination) {
        return true;
    }
}


