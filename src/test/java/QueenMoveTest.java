

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class QueenMoveTest {

    private Board board;
    private Queen queen;

    private Position from;
    private Position to;
    private boolean expectResult;

    public QueenMoveTest(Position from, Position to, boolean expectResult) {
        this.from = from;
        this.to = to;
        this.expectResult = expectResult;
    }

    @Before

    public void  setUp() {
        board = new Board();

        Position position = new Position("e4");
        String color = "white";
        boolean isAlive = true;
        boolean hasMove = true;

        queen = new Queen(color, position, isAlive, hasMove, board);


    }

    @Parameterized.Parameters
    public static Collection params() {
        return Arrays.asList(new Object[][]{
                //ruch w osich x i y
                {new Position("e4"), new Position("e8")},
                {new Position("e4"), new Position("e1")},
                {new Position("e4"), new Position("a4")},
                {new Position("e4"), new Position("i4")},
                //ruch skosny (a'la bischop)
                {new Position("e4"), new Position("i8")},
                {new Position("e4"), new Position("b1")},
                {new Position("e4"), new Position("a8")},
                {new Position("e4"), new Position("h1")},
                //ruch nieprawidłowy
                {new Position("e4"), new Position("f6")},
                {new Position("e4"), new Position("h6")},
                {new Position("e4"), new Position("f7")},
                {new Position("e4"), new Position("a5")},

        });
    }

    @Test
    public void testMoveQueen(){

        queen.setPosition(from);

        boolean result =queen.move(to);

        assertEquals(expectResult,result);

    }
    //poprawa
}